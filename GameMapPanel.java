import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import java.util.Random;


public class GameMapPanel extends Panel {
	Map map;
	GameFrame frame;
	Random rand=new Random();
	int enemyX=rand.nextInt(2,6);
	int enemyY=rand.nextInt(2,6);
	GameMapPanel(Map m,GameFrame f) { 
		map = m;
		frame=f;
	}
	
	public void paint(Graphics g) {
		for (int y = 0; y < map.sizey; y++) {
			for (int x = 0; x < map.sizex; x++) {
				if (map.hero.getPosx() == x && map.hero.getPosy() == y) {
					g.setColor(Color.black);
					g.fillRect(x * 30, y * 30, 30, 30);
				}
					else if (x == enemyX && y ==enemyY) {
						g.setColor(Color.gray);
						g.fillRect(x * 30, y * 30, 30, 30);
				} 
					else {
					g.setColor(Color.black);
					g.drawRect(x * 30, y * 30, 30, 30);
					
					
				}
			}
		}
		if (map.hero.getPosx() ==enemyX && map.hero.getPosy() == enemyY) {
			enemyX = rand.nextInt(6);
			enemyY = rand.nextInt(6);
		}


	}

}
