import java.util.Scanner;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class Programa  {

	public static void main(String[] args) {
	
		Map m;
	
		System.out.println("Welcome to the dungeon!!!");
		System.out.println("The King has send you to a mission,that will either make you a HERO or a DISSAPOINTMENT!");
		System.out.println("Your Mission is to kill the enemy and exit the dungeon alive!");
		System.out.println("You will be dropped in the start of the map and you have to find the enemy!");
		System.out.println("If you dont manage to kill him,the kingdom and its residents will be at risk!");
		
		try {
			FileInputStream fis = new FileInputStream("map.bin");
			ObjectInputStream ois = new ObjectInputStream(fis);
			m = (Map) ois.readObject();
		}  catch (IOException | ClassNotFoundException e1) {
			m = new Map(7, 7);
			Hero h = new Hero(0, 0, m);
			Enemy e=new Enemy(3,3,m);
			m.hero = h;
			m.enemy=e;
		}


		m.hero.setDamage(15);
		m.hero.health=105;
		m.enemy.setDamage(12);
		m.enemy.health=95;

		m.print(m.hero,m.enemy);
		
		GameFrame f=new GameFrame(m);
		f.map=m;
		f.setSize(500,500);
		f.setVisible(true);
		
		Scanner scan=new Scanner(System.in);
		while(true) {
			char c=scan.next().charAt(0);
			if (c == 'e') {
				try {
					FileOutputStream fos = new FileOutputStream("map.bin");
					ObjectOutputStream oos = new ObjectOutputStream(fos);
					oos.writeObject(m);
					oos.close();
					fos.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
				System.exit(0);
			}

			
			m.hero.move(c,m);
			m.print(m.hero,m.enemy);
			f.mapPanel.repaint();
			
			if((m.hero.getPosx()==m.enemy.getPosx()+1 || m.hero.getPosx()==m.enemy.getPosx()-1 || m.hero.getPosx()==m.enemy.getPosx())&& (m.hero.getPosy()==m.enemy.getPosy()-1 || m.hero.getPosy()==m.enemy.getPosy()+1 || m.hero.getPosy()==m.enemy.getPosy())) {
				System.out.println("The enemy has appeared!");
			
			while(m.enemy.health>0) {
					System.out.println("Your health: "+m.hero.health);
					System.out.println("Enemy's health: "+m.enemy.health);
					System.out.println("Pick your decision...");
					System.out.println("1.Attack!");
					System.out.println("2.Drink a health potion!");
					
					char reshenie=scan.next().charAt(0);
					if(reshenie=='1') {
						m.enemy.health=m.enemy.health-m.hero.getDamage();
						m.hero.health=m.hero.health-m.enemy.getDamage();
						System.out.println("You have hit the enemy for "+m.hero.getDamage()+" health!");
						System.out.println("You have lost "+m.enemy.getDamage()+" health!");
						if(m.hero.health<0) {
							System.out.println("You have failed to do your mission!");
							System.out.println("You lose the game!");
							break;
						}
					}
					else if(reshenie=='2') {
						if(m.hero.numberpotions>0) {
							m.hero.health=m.hero.health+m.hero.getHealthpotionheal();
							m.hero.numberpotions--;
							System.out.println("You have healed for "+m.hero.getHealthpotionheal()+" hp!");
							System.out.println("Now you have "+ m.hero.numberpotions +" left!!!");
						}
						else if(m.hero.numberpotions<=0) {
							System.out.println("You dont have any healthpotions left!");
						}
						
					}
					else if(reshenie!='1'&& reshenie!='2') {
						System.out.println("Invalid command!!!");
						System.out.println("You can only pick a number between 1 or 2!");
					}
					if(m.enemy.health<0) {
						System.out.println("Congratulations,you have defeated the enemy!");
						System.out.println("You have saved the kingdom from a potential attack!");
						System.out.println("Now type exit to leave the game!");
						String done=scan.nextLine();
						if(done=="exit") {
							System.exit(0);
						}
					}
				
					}
					
					

					
				}		
					
			}
		}
		
	}


			
		
		
	


