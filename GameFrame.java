import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
public class GameFrame extends Frame {
	GameMapPanel mapPanel;
	Map map;
	Label label,label1,label2,label3;
	TextField textField,textField1;
	TextArea textArea;
	Button button,button1;
	
	GameFrame(Map m) {
		map = m;
		GameWindowListener gwl = new GameWindowListener();
		addWindowListener(gwl);
		textArea=new TextArea();
		textArea.setLocation(300,300);
		textArea.setVisible(false);
		textArea.setSize(100,100);
		this.add(textArea);
		
		label = new Label();
		label.setLocation(310, 100);
		label.setText("Hero's HP:");
		label.setSize(100, 30);
		this.add(label);
		
		label1 = new Label();
		label1.setLocation(310, 150);
		label1.setText("Enemy's HP:");
		label1.setSize(100, 30);
		this.add(label1);
		
		label2 = new Label();
		label2.setLocation(310, 250);
		label2.setText("");
		label2.setSize(150, 30);
		this.add(label2);
		
		label3 = new Label();
		label3.setLocation(310, 50);
		label3.setText("Welcome to the dungeon!");
		label3.setSize(150, 30);
		this.add(label3);
		
		textField = new TextField();
		textField.setLocation(310, 100);
		textField.setSize(130, 30);
		textField.setText("                                 105");
		textField.setEditable(false);
		this.add(textField);
		
		textField1 = new TextField();
		textField1.setLocation(310, 150);
		textField1.setSize(130, 30);
		textField1.setText("                                  95");
		textField1.setEditable(false);
		this.add(textField1);
		
				
		button = new Button();
		button.setLabel("Attack");
		button.setLocation(50, 300);
		button.setSize(100, 30);
		GameActionListener gal = new GameActionListener(m, this);
		button.addActionListener(gal);
		this.add(button);
		
		button1 = new Button();
		button1.setLabel("Exit");
		button1.setLocation(165, 300);
		button1.setSize(100, 30);
		button1.setVisible(false);
		button1.setBackground(Color.red);
		GameActionListener1 gal1 = new GameActionListener1(this);
		button1.addActionListener(gal1);
		this.add(button1);
		
		
		mapPanel = new GameMapPanel(map, null);
		mapPanel.setLocation(50, 50);
		mapPanel.setSize(map.sizex * 30 + 1, map.sizey * 30 + 1);
		GameKeyListener gkl = new GameKeyListener(this);
		mapPanel.addKeyListener(gkl);
		GameMouseListener gml = new GameMouseListener(this);
		mapPanel.addMouseListener(gml);
		this.add(mapPanel);
		
		this.setLayout(null);
	}


	
	public void paint(Graphics g) {
		for(int y=0;y<map.sizey;y++) {
			for(int x=0;x<map.sizex;x++) {
				if(map.hero.getPosx()==x && map.hero.getPosy()==y) {
					g.fillRect(50+x*30,50+y*30,30,30);
				}
				else if(map.enemy.getPosx()==x && map.enemy.getPosy()==y) {
					g.fillRect(50+x*30,50+y*30,30,30);
				}
				else {
					g.drawRect(50+x*30, 50+y*30, 30, 30);
				}
			}
		}
	}
}
