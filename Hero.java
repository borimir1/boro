import java.io.Serializable;
public class Hero extends Coordinates  implements Serializable  {
		Map map;
		GameFrame frame;
		private int healthpotionheal=20;
		int numberpotions=3;
		int health;
		private int damage;
		private Coordinates pos;
		public int getPosx() {
			return pos.getX();
		}
		
		public int getPosy() {
			return pos.getY();
		}
		Hero(int x,int y,Map  m){
			super(x,y);
			pos=new Coordinates(x,y);
			map=m;
		}
		public void setPos(int x,int y) {
			if(x>=0 && x<map.sizex && y>=0 && y<map.sizey) {
				//if(map.enemy.getPosx()==x && map.enemy.getPosy()==y) {
					//System.out.println("You cant go there");
				//}
				if((map.enemy.getPosx()+1==x || map.enemy.getPosx()-1==x || map.enemy.getPosx()==x) && (map.enemy.getPosy()-1==y || map.enemy.getPosy()+1==y || map.enemy.getPosy()==y)){
					pos=new Coordinates(x,y);
					//System.out.println("The enemy has appeared!");
				}
				else {
					pos = new Coordinates(x,y);
				}
			}
		}
		
		public int getDamage() {
			return damage;
		}
		public void setDamage(int a) {
			if(a>=0) {
				damage=a;
			}
			else {
				System.out.println("Invalid command!");
				System.out.println("The number that you put in must be higher or equal to 0!");
			}
		}
	
		void move(char c,Map m) {
			switch(c) {
			case 'w':
				if (getPosy()!= 0) {
					setPos(getPosx(),getPosy()-1);
				}
				break;
			case 's':
				if (getPosy()!= map.sizey - 1) {
					setPos(getPosx(),getPosy()+1);
				}
				break;
			case 'a':
				if (getPosx() != 0) {
					setPos(getPosx()-1,getPosy());
				}
				break;
			case 'd':
				if (getPosx()!= map.sizex - 1) {
					setPos(getPosx()+1,getPosy());
				}
				break;
		}

	}
	
	
	public int getHealthpotionheal() {
		return healthpotionheal;
	}
	public void setHealthpotionheal(int c) {
		if(c>=0) {
			healthpotionheal=c;
		}
		else {
			System.out.println("Invalid command!");
			System.out.println("The number that you put in must be higher or equal to 0!");
		}
	}


	
	
	
    
}
